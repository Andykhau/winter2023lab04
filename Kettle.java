public class Kettle
{
	private String colour;
	private String brand;
	private int maxLiter;
	
	//constructor
	public Kettle(String colour, String brand, int maxLiter)
	{
		this.colour = colour;
		this.brand = brand;
		this.maxLiter = maxLiter;
	}
	
	//get
	public String getColour()
	{
		return this.colour;
	}
	
	public String getBrand()
	{
		return this.brand;
	}
	
	public int getMaxLiter()
	{
		return this.maxLiter;
	}
	//set
	public void setColour(String newColour)
	{
		this.colour = newColour;
	}
	
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}
	public void setMaxLiter(int newMaxLiter)
	{
		this.maxLiter = newMaxLiter;
	}
	
	
	public void information() {
		System.out.println("You have chosen the colour: " + this.colour);
		System.out.println("You have chosen the brand: " + this.brand);
		System.out.println("You have chosen the size of: " + this.maxLiter);
	}
	
	public void boilTimer(int seconds)
	{
		System.out.println("The water will be ready in:");
		for(int i = seconds; i >= 0; i--)
		{
			System.out.println(i);
		}
		System.out.println("DING!!!");
		System.out.println("The water is READY!");
	}
	
	public void waterQuantity(double maxLiter)
	{
		if(maxLiter <= 2)
		{
			System.out.println("You can find a kettle that contain that much water.");
		}
		else
		{
			System.out.println("You will have a hard time finding a kettle of that size.");
		}
	}
}